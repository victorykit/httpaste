.. include:: README.rst

.. toctree::
    :maxdepth: 1
    :caption: Guides

    guide/getting-started
    guide/advanced-usage
    guide/backend
    guide/cli



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
