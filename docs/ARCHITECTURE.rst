Architecture
============

.. only:: not readme

    .. uml:: architecture.uml

This program's design resembles a Resource-Method-Representation architecture pattern. This is due to the design choice of making it API-oriented, instead of
domain-oriented. Therefore 


Router


Controller


Model


Backend
