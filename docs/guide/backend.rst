Backend
=======

The backend can be configured within the `[backend]` section of the configuration file.

SQLite
------

.. autoclass:: httpaste.backend.sqlite.Parameters
    :members:

Filesystem
----------

.. autoclass:: httpaste.backend.file.Parameters
    :members: