# httpaste - versatile HTTP pastebin

![](docs/_assets/images/favpng_parrot-royalty-free-cartoon.png)

**NOTE**: httpaste is publicly hosted at [httpaste.it](http://httpaste.it) and as a hidden Tor service ([https://paste77ubkwxy4fqezffsmthxdh3xerwi72tlsw2mch7ecjhw2xn7iyd.onion](https://paste77ubkwxy4fqezffsmthxdh3xerwi72tlsw2mch7ecjhw2xn7iyd.onion)).
Both services are to be considered evaluatory, as long as the source code
is in pre-release. Regarding voidance of pre-release status, see [Open Issues](https://victorykit.atlassian.net/issues/?jql=project%20%3D%20HTTPASTE%20AND%20fixVersion%20in%20(1.1.0-beta%2C%201.2.0-beta%2C%201.3.0)), for more information.

This program offers an HTTP interface for storing public and private data
(a.k.a. pastes), commonly referred to as a pastebin application. It is inspired by [sprunge.us](http://sprunge.us) and [ix.io](http://ix.io/). It can be hosted through WSGI, CGI, Fast
CGI, or as a standalone evaluation server. It offers multiple storage backends,
such as a filesystem backend, SQLite backend, or MySQL backend.

Public data can be accessed through an URL, where as private pastes
additionally require HTTP basic authentication. Creation of authentication
credentials happens on the fly, there is no sign-up process. Public pastes can
only be accessed by knowing their paste ids, they are not listed on any index,
since it isn’t technically possible (by design).

All pastes are symetrically encrypted server-side with an HMAC derived key and
SHA-256 hashing, a server-side salt and a randomly generated password. Public
paste’s passwords are derived from their ids. Private paste’s passwords are
randomly generated and stored inside a symetrically encrypted personal
database, with the encryption key also being derived through the same HMAC
mechanism, where the HTTP basic authentication credentials act as the master
password.

Paste ids, usernames, and any other identifiable attributes are only stored
inside storage backends as keyed and salted BLAKE2 hashes.

The program supports output formatting for syntax highlighting (powered by
[pygments](https://pygments.org/)), as well as MIME type output manipulation, and input encoding.
The program can therefore serve as a minimalist, anonymous object storage for
small data.

Minute-based and ‘burn-after-read’ paste expiration are also supported.

# Getting Started

## Install

```shell
$ python3 -m pip install httpaste-victorykit
$ httpaste --help
```

## Create Configuration

```shell
$ httpaste default-config --dump myconfig.ini
```

**NOTE**: The default configuration creates an in-memory SQLite backend, which is not
suitable for WWW deployments. Visit backend, for more
information on configuring the backend.

## Run a Local Evaluation Server

```shell
$ httpaste standalone --config myconfig.ini --port 8080
```

## Publish a Private Paste

```shell
$ echo 'My first private paste' | curl -F 'data=<-' -u myusername:mypassword http://localhost:8080/paste/private
http://localhost:8080/paste/private/UALUA9
```

**NOTE**: If the user does not exist, they will be created upon authentication.

## Retrieve a Private Paste

```shell
$ curl -u myusername:mypassword http://localhost:8080/paste/private/UALUA9
My first private paste
```

## Publish a Public Paste

```shell
$ echo 'My first public paste' | curl -F 'data=<-' http://localhost:8080/paste/public
http://localhost:8080/paste/public/X4L39J
```

## Retrieve a Public Paste

```shell
$ curl http://localhost:8080/paste/public/X4L39J
My first public paste
```

### Documentation

The documentation can be found under [https://victorykit.bitbucket.io/httpaste/](https://victorykit.bitbucket.io/httpaste/).

### Licensing

Copyright (C) 2021  Tiara Rodney (victoryk.it)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <[https://www.gnu.org/licenses/](https://www.gnu.org/licenses/)>.

This program uses licensed third-party software.

### More Information


* [Architecture](ARCHITECTURE.md)


* [Contribution Guidelines](CONTRIBUTING.md)
